module fr.afpa {
    requires transitive javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    opens fr.afpa to javafx.fxml;
    exports fr.afpa;
}
