package fr.afpa;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;

public class PersonsController {
    @FXML
    private TextField tfFirstName;
    @FXML
    private TextField tfLastName;
    @FXML
    private TextField tfCity;
    @FXML
    private TableView<Person> tableView;
    @FXML
    private TableColumn<Person, String> colFirstName;
    @FXML
    private TableColumn<Person, String> colLastName;
    @FXML
    private TableColumn<Person, String> colCity;
    @FXML
    private Button save;
    @FXML
    private Button cancel;
    @FXML
    private Button delete;

    private ObservableList<Person> persons = FXCollections.observableArrayList();


    @FXML
    public void save(ActionEvent event) {
        Person new1 = new Person(tfFirstName.getText(), tfLastName.getText(), tfCity.getText());
        persons.add(new1);
        tableView.setItems(persons);
        tfFirstName.clear();
        tfLastName.clear();
        tfCity.clear();

    }

    @FXML
    private void cancel(ActionEvent event) {
        tfFirstName.clear();
        tfLastName.clear();
        tfCity.clear();
    }

    @FXML
    private void delete(ActionEvent event) {
        Person selectedPerson = tableView.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            persons.remove(selectedPerson);
            tableView.setItems(persons);
    }
    }
    @FXML
    public void initialize() {
        persons.add(new Person("Josh", "Homme", "Joshua Tree"));
        persons.add(new Person("Dave", "Grohl", "Warren"));
        persons.add(new Person("Robert", "Trujillo", "Santa Monica"));
        tableView.setItems(persons);


        colFirstName.setCellValueFactory(tfFirstName -> tfFirstName.getValue().firstNameProperty());
        colLastName.setCellValueFactory(tfFirstName -> tfFirstName.getValue().lastNameProperty());
        colCity.setCellValueFactory(tfFirstName -> tfFirstName.getValue().cityProperty());


    }

}
