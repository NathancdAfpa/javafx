package fr.afpa;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        
        // récupération de l'URL locale (adresse du fxml)
        URL url = App.class.getResource("persons.fxml");
        // instanciation du FXML loader
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        
        // chargement du FXML -> le FXML loader instancie le contrôlleur associé
        Parent parent = fxmlLoader.load();

        scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();

        
    }

    public static void main(String[] args) {
        launch();
    }

}