module fr.afpa.javafx {
    requires transitive javafx.controls;
    requires javafx.fxml;
    opens fr.afpa.javafx to javafx.fxml;
    exports fr.afpa.javafx;
}
