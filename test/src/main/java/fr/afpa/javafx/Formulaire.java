package fr.afpa.javafx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.event.*;

/**
 * Application de démonstration
 */
public class Formulaire extends Application {

    private TextInputControl motDePasse;

    @Override
    public void start(Stage stage) {

        // Instanciation des composants graphique
        Label entreeUtilisateurLabel = new Label("Entrée utilisateur :");
        Label motDePasseLabel = new Label("Copie de l'entrée :");

        TextField entreeUtilisateur = new TextField("Saisissez un texte");

        entreeUtilisateur.setEditable(true);
        entreeUtilisateur.setDisable(false);
        

        TextField motDePasse = new TextField("Saisissez un texte");

        motDePasse.setEditable(true);
        motDePasse.setDisable(false);
        
        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(5, 10, 5, 10));

        Button recopier = new Button("Recopier");

        recopier.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // TODO Auto-generated method stub
                entreeUtilisateur.getText();
                entreeUtilisateur.setText(entreeUtilisateur.getText());
            }
        }

        );

        recopier.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                motDePasse.getText();
                motDePasse.setText(entreeUtilisateur.getText());
            }
        }

        );

        Button effacer = new Button("Effacer");
        effacer.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                motDePasse.clear();
                entreeUtilisateur.clear();
            }
        });

        Button quitter = new Button("Quitter");
        quitter.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
            }

        });

        // Création d'une classe locale pour la gestion de l'évènement "click"
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String userName = entreeUtilisateur.getText();
                String password = motDePasse.getText();
                // if (userName.isEmpty() || password.isEmpty()) {
                // messageLabel.setText("Veuillez saisir tous les champs.");
                // messageLabel.setStyle("-fx-text-fill: WHITE; -fx-background-color: RED");
                // } else {
                // messageLabel.setText("Connexion acceptée pour l'utilisateur " + userName);
                // messageLabel.setStyle("-fx-text-fill: WHITE; -fx-background-color: GREEN");
                // }
            }
        };

        recopier.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
        effacer.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
        quitter.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);

        // Instanciation d'un pane grille pour organisation des composants graphiques
        GridPane gridPane = new GridPane();
        gridPane.setHgap(9);
        gridPane.setVgap(5);

        gridPane.setStyle("-fx-background-color: BEIGE");

        // ajout de "padding" pour l'écarter du bord
        gridPane.setPadding(new Insets(22, 15, 20, 25));

        gridPane.add(entreeUtilisateurLabel, 0, 0);
        GridPane.setVgrow(entreeUtilisateurLabel, Priority.ALWAYS);
        gridPane.add(entreeUtilisateur, 1, 0);
        GridPane.setHgrow(entreeUtilisateur, Priority.ALWAYS);

        gridPane.add(motDePasseLabel, 0, 2);
        GridPane.setVgrow(motDePasseLabel, Priority.ALWAYS);

        gridPane.add(motDePasse, 1, 2);
        GridPane.setHgrow(motDePasse, Priority.ALWAYS);
        // gridPane.setGridLinesVisible(true);

        // Ajout du message d'information
        messageLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(messageLabel, 1, 2);
        GridPane.setColumnSpan(messageLabel, 2);

        // Ajout du bouton
        // avec gestion de la taille du button et du "span" sur plusieurs colonnes
        // On autorise le bouton à prendre toute la place disponible
        recopier.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(recopier, 4, 0);
        GridPane.setColumnSpan(recopier, 11);

        effacer.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(effacer, 4, 1);
        GridPane.setColumnSpan(effacer, 11);

        quitter.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(quitter, 4, 2);
        GridPane.setColumnSpan(quitter, 11);

        // Intanciation d'un "pane" racine
        // permet de faire "coller" au bord ce qu'il contient
        FlowPane root = new FlowPane();
        // Ajout du "gridPane" à la liste des enfants de la racine
        root.getChildren().add(gridPane);
        // paramétrage du "pane" racine afin de faire coller la grille
        AnchorPane.setLeftAnchor(gridPane, 0.0);
        AnchorPane.setRightAnchor(gridPane, 10.0);
        AnchorPane.setTopAnchor(gridPane, 0.0);
        AnchorPane.setBottomAnchor(gridPane, 0.0);

        // Création de la scène à afficher (le contenu de la fenêtre)
        Scene scene = new Scene(root);
        // Ajout de la scène à la fenêtre
        stage.setScene(scene);
        // Affichage de la fenêtre
        stage.show();
        stage.setTitle("                                              Formulaire");
    }

    private static void exit() {
    }

    public static void main(String[] args) {
        launch();
    }

    // Classe INTERNE de gestion d'évènement
    // -> à titre d'exemple
    // public class ConnexionHandler implements EventHandler<MouseEvent> {
    // @Override
    // public void handle(MouseEvent event) {
    // System.out.println("Bonjour !");
    // }
    // }
}
