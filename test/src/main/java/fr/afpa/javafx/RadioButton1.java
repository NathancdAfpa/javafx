package fr.afpa.javafx;

import javafx.event.*;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class RadioButton1 extends Application {

    @Override
    public void start(Stage stage) {

        Label aSaisirLabel = new Label(" Saisissez votre texte");
        Label aBonjourLabel = new Label("bonjour tout le monde !");
        Label rouge = new Label("Rouge");
        rouge.setVisible(false);
        Label vert = new Label("Vert");
        vert.setVisible(false);
        Label bleu = new Label("Bleu");
        bleu.setVisible(false);
        aBonjourLabel.setPadding(new Insets(10, 10, 10, 10));
        aBonjourLabel.setStyle(
                "-fx-background-color: blue; -fx-text-fill: red; -fx-font-size: 16px; -fx-font-family: CALIBRI");
        TextField aSaisir = new TextField("Bonjour tout le monde !");
        aSaisir.setEditable(true);
        TitledPane fond = new TitledPane();
        fond.setText("Fond");
        fond.setVisible(false);
        // fond.setPrefSize(110.0, 110.0);
        // fond.setMaxWidth(90);
        TitledPane setLabel = new TitledPane();
        setLabel.setText("Paramètres label");
        // setLabel.setPrefSize(150.0, 110.0);
        // setLabel.setMaxWidth(150);
        TitledPane couleur = new TitledPane();
        couleur.setText("Couleur des caractères");
        couleur.setVisible(false);
        // couleur.setPrefSize(110.0, 110.0);
        // couleur.setMaxWidth(70);
        TitledPane casse = new TitledPane();
        casse.setText("Casse");
        casse.setVisible(false);
        // casse.setPrefSize(87.0, 87.0);
        // casse.setMaxWidth(100);

        CheckBox check1 = new CheckBox("Couleur de fond");

        CheckBox check2 = new CheckBox("Couleur des caractères");

        CheckBox check3 = new CheckBox("Casse");

        ToggleGroup group = new ToggleGroup();

        RadioButton rdb = new RadioButton("Rouge");
        rdb.setToggleGroup(group);
        rdb.setVisible(false);

        RadioButton rdb1 = new RadioButton("Vert");
        rdb1.setToggleGroup(group);
        rdb1.setVisible(false);

        RadioButton rdb2 = new RadioButton("Bleu");
        rdb2.setToggleGroup(group);
        rdb2.setVisible(false);

        RadioButton rdb3 = new RadioButton("Majuscule");
        rdb3.setToggleGroup(group);
        rdb3.setVisible(false);

        RadioButton rdb4 = new RadioButton("Minuscule");
        rdb4.setToggleGroup(group);
        rdb4.setVisible(false);

        Slider slider = new Slider(0, 255, 127.5);
        slider.setVisible(false);
        slider.setMaxSize(95, 95);
        slider.setShowTickMarks(false);
        slider.setShowTickLabels(false);
        slider.setMajorTickUnit(1);

        Slider slider1 = new Slider(0, 255, 127.5);
        slider1.setVisible(false);
        slider1.setMaxSize(95, 95);
        slider.setShowTickMarks(false);
        slider.setShowTickLabels(false);
        slider.setMajorTickUnit(1);
        
        Slider slider2 = new Slider(0, 255, 127.5);
        slider2.setVisible(false);
        slider2.setMaxSize(95, 95);
        slider.setShowTickMarks(false);
        slider.setShowTickLabels(false);
        slider.setMajorTickUnit(1);

        aSaisir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                aBonjourLabel.getText();
                aBonjourLabel.setText(aSaisir.getText());
            }
    } );
    
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (check2.isSelected()) {
                    couleur.setVisible(true);
                    slider.setVisible(true);
                    slider1.setVisible(true);
                    slider2.setVisible(true);
                    rouge.setVisible(true);
                    vert.setVisible(true);
                    bleu.setVisible(true);

                } else {
                    couleur.setVisible(false);
                    slider.setVisible(false);
                    slider1.setVisible(false);
                    slider2.setVisible(false);
                    rouge.setVisible(false);
                    vert.setVisible(false);
                    bleu.setVisible(false);
                }
            }
        };

        EventHandler<MouseEvent> eventHandler1 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (check1.isSelected()) {
                    fond.setVisible(true);
                    rdb.setVisible(true);
                    rdb1.setVisible(true);
                    rdb2.setVisible(true);
                } else {
                    fond.setVisible(false);
                    rdb.setVisible(false);
                    rdb1.setVisible(false);
                    rdb2.setVisible(false);
                }

            }
        };

        EventHandler<MouseEvent> eventHandler2 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (check3.isSelected()) {
                    casse.setVisible(true);
                    rdb3.setVisible(true);
                    rdb4.setVisible(true);
                } else {
                    casse.setVisible(false);
                    rdb3.setVisible(false);
                    rdb4.setVisible(false);
                }

            }
        };

        EventHandler<MouseEvent> eventHandler3 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (rdb.isSelected()) {
                    aBonjourLabel.setStyle(
                        "-fx-background-color: RED; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                } else {
                    aBonjourLabel.setStyle(
                "-fx-background-color: BLUE; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                }

            }
        };

        EventHandler<MouseEvent> eventHandler4 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (rdb1.isSelected()) {
                    aBonjourLabel.setStyle(
                        "-fx-background-color: GREEN; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                } else {
                    aBonjourLabel.setStyle(
                "-fx-background-color: BLUE; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                }

            }
        };

        EventHandler<MouseEvent> eventHandler5 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (rdb2.isSelected()) {
                    aBonjourLabel.setStyle(
                        "-fx-background-color: BLUE; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                } else {
                    aBonjourLabel.setStyle(
                "-fx-background-color: BLUE; -fx-text-fill: RED; -fx-font-size: 16px; -fx-font-family: CALIBRI");
                }
            }
        };

        EventHandler<MouseEvent> eventHandler6 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (rdb3.isSelected()) {
                    aBonjourLabel.setText(aBonjourLabel.getText().toUpperCase());
                       
                } else {
                    aBonjourLabel.setText(aBonjourLabel.getText().toLowerCase());
                
                }
            }
        };

        EventHandler<MouseEvent> eventHandler7 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (rdb4.isSelected()) {
                    aBonjourLabel.setText(aBonjourLabel.getText().toLowerCase());
                } else {
                    aBonjourLabel.setText(aBonjourLabel.getText().toUpperCase());
                }
            }
        };
        
        EventHandler<MouseEvent> eventHandler8 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                slider.valueProperty().getValue();
               
           

            }
        };

        
        

        EventHandler<MouseEvent> eventHandler9 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                slider1.valueProperty().getValue();

            }
        };

        EventHandler<MouseEvent> eventHandler10 = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                slider2.valueProperty().getValue();

               
            }
        };

        slider.valueProperty().addListener((obs, oldVal, newVal) -> {       
            int rougeValue =  (int) slider.getValue();
            int vertValue =  (int) slider1.getValue();
            int bleuValue =  (int) slider2.getValue();
            aBonjourLabel.setTextFill(Color.rgb(rougeValue, vertValue, bleuValue));
        });

        slider1.valueProperty().addListener((obs, oldVal, newVal) -> {
            int rougeValue =  (int) slider.getValue();
            int vertValue =  (int) slider1.getValue();
            int bleuValue =  (int) slider2.getValue();
            aBonjourLabel.setTextFill(Color.rgb(rougeValue, vertValue, bleuValue));
            // aBonjourLabel.setTextFill(Color.rgb(0, 255, 0, newVal.doubleValue()));

        });

        slider2.valueProperty().addListener((obs, oldVal, newVal) -> {
            int rougeValue =  (int) slider.getValue();
            int vertValue =  (int) slider1.getValue();
            int bleuValue =  (int) slider2.getValue();
            aBonjourLabel.setTextFill(Color.rgb(rougeValue, vertValue, bleuValue));
            // aBonjourLabel.setTextFill(Color.rgb(0, 0, 255, newVal.doubleValue()));
        });

        String recup = doubleValueToRGB(100, 100, 100);
        

        check2.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
        check1.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler1);
        check3.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler2);
        rdb.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler3);
        rdb1.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler4);
        rdb2.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler5);
        rdb3.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler6);
        rdb4.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler7);
        slider.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler8);
        slider1.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler9);
        slider2.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler10);
       
        VBox vbox1 = new VBox(10, aSaisirLabel, aSaisir, aBonjourLabel);
        vbox1.setPadding(new Insets(5, 5, 5, 5));

        VBox vbox2 = new VBox(10, setLabel, check1, check2, check3);
        vbox2.setPadding(new Insets(10, 10, 10, 10));

        

        // VBox vbox4 = new VBox(10, fond, rdb, rdb1, rdb2);
        // vbox4.setPadding(new Insets(10, 10, 10, 10));

        VBox vbox3 = new VBox(10, fond, rdb, rdb1, rdb2);
        vbox3.setPadding(new Insets(10, 10, 10, 10));

        VBox vbox4 = new VBox(10, couleur, rouge, vert, bleu);
        vbox4.setPadding(new Insets(10, 10, 10, 10));



        VBox vbox7 = new VBox(13, slider, slider1, slider2);
        vbox7.setPadding(new Insets(48, -50, 0, -120));

        VBox vbox8 = new VBox(10, casse, rdb3, rdb4);
        vbox8.setPadding(new Insets(10, 10, 10, 10));

        

        HBox hbox = new HBox(10, vbox1, vbox2);
        hbox.setPadding(new Insets(0, 0, 0, 0));

        HBox hbox2 = new HBox(0, vbox3, vbox4, vbox8);
        hbox2.setPadding(new Insets(0, 0, 0, 0));


        VBox grosseVbox = new VBox(10, hbox, hbox2);
        grosseVbox.setPadding(new Insets(5, 5, 5, 5));



        FlowPane root = new FlowPane();

        root.getChildren().add(grosseVbox);
        root.getChildren().add(hbox);
        root.getChildren().add(hbox2);
       
        root.getChildren().add(vbox1);
        root.getChildren().add(vbox2);
        root.getChildren().add(vbox3);
        root.getChildren().add(vbox4);
        root.getChildren().add(vbox7);
        root.getChildren().add(vbox8);
       

        // root.getChildren().addAll(rdb, rdb1, rdb2, rdb3, rdb4);
        // root.getChildren().addAll(rdb, rdb1, rdb2);

        AnchorPane.setLeftAnchor(grosseVbox, 0.0);
        AnchorPane.setRightAnchor(grosseVbox, 10.0);
        AnchorPane.setTopAnchor(grosseVbox, 0.0);
        AnchorPane.setBottomAnchor(grosseVbox, 0.0);

        AnchorPane.setLeftAnchor(hbox, 0.0);
        AnchorPane.setRightAnchor(hbox, 10.0);
        AnchorPane.setTopAnchor(hbox, 0.0);
        AnchorPane.setBottomAnchor(hbox, 0.0);

        AnchorPane.setLeftAnchor(hbox2, 0.0);
        AnchorPane.setRightAnchor(hbox2, 10.0);
        AnchorPane.setTopAnchor(hbox2, 0.0);
        AnchorPane.setBottomAnchor(hbox2, 0.0);

       


        AnchorPane.setLeftAnchor(vbox1, 0.0);
        AnchorPane.setRightAnchor(vbox1, 10.0);
        AnchorPane.setTopAnchor(vbox1, 0.0);
        AnchorPane.setBottomAnchor(vbox1, 0.0);

        AnchorPane.setLeftAnchor(vbox2, 0.0);
        AnchorPane.setRightAnchor(vbox2, 10.0);
        AnchorPane.setTopAnchor(vbox2, 0.0);
        AnchorPane.setBottomAnchor(vbox2, 0.0);

        AnchorPane.setLeftAnchor(vbox3, 0.0);
        AnchorPane.setRightAnchor(vbox3, 10.0);
        AnchorPane.setTopAnchor(vbox3, 0.0);
        AnchorPane.setBottomAnchor(vbox3, 0.0);

        AnchorPane.setLeftAnchor(vbox4, 0.0);
        AnchorPane.setRightAnchor(vbox4, 10.0);
        AnchorPane.setTopAnchor(vbox4, 0.0);
        AnchorPane.setBottomAnchor(vbox4, 0.0);

       

        AnchorPane.setLeftAnchor(vbox7, 0.0);
        AnchorPane.setRightAnchor(vbox7, 10.0);
        AnchorPane.setTopAnchor(vbox7, 0.0);
        AnchorPane.setBottomAnchor(vbox7, 0.0);

        AnchorPane.setLeftAnchor(vbox8, 0.0);
        AnchorPane.setRightAnchor(vbox8, 10.0);
        AnchorPane.setTopAnchor(vbox8, 0.0);
        AnchorPane.setBottomAnchor(vbox8, 0.0);


        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();
        stage.setTitle("CheckBox - RadioButton - Slider");
    }

    public static String doubleValueToRGB(double r, double g, double b) {
        return "rgb(" + r + "," + g + "," + b + ")";       
    }

    private TitledPane TitledPane() {
        return null;
    }

    private void setToggleGroup(ToggleGroup group) {
    }

    private void setSelected(boolean b) {
    }

    private void setPadding(int i) {
    }

    private void setPrefHeight(int i) {
    }

    private static void exit() {
    }

    public static void main(String[] args) {
        launch();
    }

}