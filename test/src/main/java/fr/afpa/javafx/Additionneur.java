package fr.afpa.javafx;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


public class Additionneur extends Application {
    private TextArea textField;
    private int sum;

    @Override
    public void start(Stage primaryStage) {
        textField = new TextArea();
        textField.setEditable(false);
        
       

        ScrollPane scrollPane = new ScrollPane(textField);
        scrollPane.setFitToWidth(true);
        scrollPane.setPrefHeight(200);


        GridPane buttonPane = new GridPane();
        buttonPane.setHgap(10);
        buttonPane.setVgap(10);
        buttonPane.setPadding(new Insets(0, 0, 0, 50));

        for (int i = 0; i < 10; i++) {
            Button numberButton = new Button(Integer.toString(i));
            numberButton.setOnAction(e -> addToTextField(numberButton.getText()));
            buttonPane.add(numberButton, i % 5, i / 5);
        }

        Button calculateButton = new Button("Calculer");
        calculateButton.setOnAction(e -> calculateSum());

        Button clearButton = new Button("Vider");
        clearButton.setOnAction(e -> clearTextField());

        buttonPane.add(calculateButton, 0, 2, 2, 1);
        buttonPane.add(clearButton, 3, 2, 2, 1);

        BorderPane root = new BorderPane();
        root.setCenter(scrollPane);
        root.setBottom(buttonPane);

        Scene scene = new Scene(root, 265, 220);
        primaryStage.setScene(scene);
        primaryStage.setTitle("                                        Additionneur");
        primaryStage.show();
    }

    private void addToTextField(String text) {
        if (text.matches("\\d")) { // only allow digits
            textField.appendText(text + " + ");
        }
    }

    private void calculateSum() {
        String[] numbers = textField.getText().split("\\s*\\+\\s*");
        sum = 0;
        for (String number : numbers) {
            if (number.matches("\\d+")) {
                sum += Integer.parseInt(number);
            }
        }
        textField.appendText("= " + sum);
    }

    private void clearTextField() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Êtes-vous sûr de vouloir vider le champ de texte ?");
        alert.setHeaderText(null);
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    textField.setText("");
                    sum = 0;
                });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
