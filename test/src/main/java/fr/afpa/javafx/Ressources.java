package fr.afpa.javafx;

import java.net.URL;   
  
import java.lang.*;  
public class Ressources {  
   public static void main(String[] args) throws Exception {  
      Ressources obj = new Ressources();  
      Class class1 = obj.getClass();  
      URL url = class1.getResource("bouton_bas_png");  
      System.out.println("Value URL = " + url);  
       URL url2 = class1.getResource("bouton_haut_png");  
      System.out.println("Value URL = " + url2);  
   }  
}  