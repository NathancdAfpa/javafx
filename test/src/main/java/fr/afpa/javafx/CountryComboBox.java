package fr.afpa.javafx;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Locale;
import javafx.event.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CountryComboBox<E> extends Application {

  private ObservableList<Country> comboBoxList;
  private ObservableList<Country> countryList;

  @Override
  public void start(Stage stage) throws FileNotFoundException {

    Label paysDispo = new Label("Pays disponible :");
    ComboBox<Country> countryComboBox = new ComboBox<>();
    countryComboBox.getItems().addAll();
    ArrayList<Country> recup = getCountriesList();
    comboBoxList = FXCollections.observableArrayList();

    for (int i = 0; i < recup.size(); i++) {
      comboBoxList.add(recup.get(i));

    }
    Button addBox = new Button(">");
    Button addAllBox = new Button(">>");
    Button deleteBox = new Button("<");
    Button deleteAllBox = new Button("<<");
   
    Image imageUp = new Image(getClass().getResourceAsStream("bouton_haut.png"));
    ImageView imageViewUp = new ImageView(imageUp);
    Button upButton = new Button("", imageViewUp);

    imageViewUp.setFitWidth(15);
    imageViewUp.setFitHeight(20);

    Image imageDown = new Image(getClass().getResourceAsStream("bouton_bas.png"));
    ImageView imageViewDown = new ImageView(imageDown);
    Button downButton = new Button("", imageViewDown);

    imageViewDown.setFitWidth(15);
    imageViewDown.setFitHeight(20);


    Button exit = new Button("Quitter");
    ListView<Country> countryList = new ListView<>();
    countryList.setPrefWidth(200);
    countryList.setMaxSize(200, 200);

    ObservableList<Country> observableList = FXCollections.observableArrayList();
    ComboBox<Country> comboBox = new ComboBox<>();
    comboBox.setItems(comboBoxList);
    countryComboBox.setItems(comboBoxList);

    for (Country item : comboBox.getItems()) {
      observableList.add(item);
    }

    VBox vbox1 = new VBox(paysDispo, countryComboBox);
    vbox1.setPadding(new Insets(8, 0, 0, 0));
    vbox1.setSpacing(10);

    VBox vbox2 = new VBox(addBox, addAllBox, deleteBox, deleteAllBox);
    vbox2.setPadding(new Insets(35, 0, 0, 0));
    vbox2.setSpacing(10);

    HBox hbox = new HBox(upButton, downButton);
    hbox.setAlignment(Pos.CENTER_RIGHT);
    hbox.setSpacing(10);

    HBox hboxExit = new HBox(exit);
    hboxExit.setAlignment(Pos.CENTER_RIGHT);
    hboxExit.setSpacing(10);

    VBox vbox3 = new VBox(hbox, countryList, hboxExit);

    vbox3.setSpacing(10);

    HBox grosseHbox = new HBox(vbox1, vbox2, vbox3);
    grosseHbox.setPadding(new Insets(20, 20, 20, 20));
    grosseHbox.setSpacing(10);

    Scene scene = new Scene(grosseHbox);
    stage.setScene(scene);
    stage.show();

    addBox.setOnAction(e -> {
      Country selectedCountry = countryComboBox.getSelectionModel().getSelectedItem();
      if (selectedCountry != null) {
        countryList.getItems().addAll(selectedCountry);
        comboBoxList.remove(selectedCountry);
      }
    });

    addAllBox.setOnAction(e -> {
      countryList.getItems().addAll(countryComboBox.getItems());
      comboBoxList.removeAll(comboBoxList);
    });

    deleteBox.setOnAction(e -> {
      int selectedIndex = countryList.getSelectionModel().getSelectedIndex();
      if (selectedIndex != -1) {
        Country selectedCountry = countryList.getItems().remove(selectedIndex);
        comboBoxList.add(selectedCountry);
      }
    });

    deleteAllBox.setOnAction(e -> {
      comboBoxList.addAll(countryList.getItems());
      countryList.getItems().clear();
    });
    
    

    exit.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        Platform.exit();
      }

    });

    upButton.setOnAction(e -> {
      int selectedIndex = countryList.getSelectionModel().getSelectedIndex();
      if (selectedIndex > 0) {
        Country selectedCountry = countryList.getItems().remove(selectedIndex);
        countryList.getItems().add(selectedIndex - 1, selectedCountry);
        countryList.getSelectionModel().select(selectedIndex - 1);
      }
    });

    downButton.setOnAction(e -> {
      int selectedIndex = countryList.getSelectionModel().getSelectedIndex();
      if (selectedIndex < countryList.getItems().size() - 1) {
        Country selectedCountry = countryList.getItems().remove(selectedIndex);
        countryList.getItems().add(selectedIndex + 1, selectedCountry);
        countryList.getSelectionModel().select(selectedIndex + 1);
      }
    });

  }

  public static ArrayList<Country> getCountriesList() {
    ArrayList<Country> countries = new ArrayList<Country>();
    String[] countryCodes = Locale.getISOCountries();
    for (String countryCode : countryCodes) {
      Locale obj = new Locale("", countryCode);
      countries.add(new Country(obj.getDisplayCountry(), obj.getISO3Country()));
    }
    return countries;
  }
  

  public static void main(String[] args) {
    launch(args);
  }

}
